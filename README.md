快速开始微信小程序
===============

目录结构：
~~~
wechat-mini-init   项目目录
├─images           图片资源目录
│  ├─icons         图标资源目录
│  └─tabbars       tabbar图标目录
│
├─listener              监听器目录
│  ├─app.js             app 相关监听
│  ├─http.js            http 相关监听
│  ├─index.js            初始化相关监听器
│  └─...                其他监听器
│
├─pages                 页面目录
│  ├─examples           小例子目录，因为不定期更新，所以就不一一列出了
│  ├─index              首页
│  ├─user               会员中心
│  │   ├─auth           用户授权页面
│  │   └─...            会员中心其他页面
│  └─...                其他页面
│
├─style                 样式目录
│
├─tests                 测试目录，可以直接删除
│
├─utils                 JS相关工具目录
│  ├─api.js             封装的常用后台api
│  ├─http.js            网络请求封装
│  ├─listener.js        监听器
│  ├─native-extension.js原生JS扩展，不要改动
│  ├─qs.js              详情请参考:https://www.npmjs.com/package/qs
│  ├─util.js            封装的一些快捷方法
│  └─validate.js        验证类，使用方法请参考:https://gitee.com/lxsea/wechat-mini-init/blob/master/tests/validate.test.js
│
├─app.js                小程序启动文件
├─app.json              小程序公共配置文件
├─app.wxss              小程序公共样式文件
├─project.config.json   小程序项目配置文件
├─config.js             开发者自定义应用配置文件
├─README.md             本文件
~~~

## 命名规范

`wechat-mini-init`使用的是ES6+语法(请在小程序开发者工具开启ES6支持：右上角详情->勾选ES6转ES5)，至于为何使用ES6，是因为懒的写ES5代码，在开发的时候请注意如下规范：

### 目录和文件
*   推荐：目录不强制规范，驼峰和小写+下划线模式；
*   严格：类名采用驼峰法命名（首字母大写），类文件名字母小写，多单词之间用横线隔开；

### 类、属性命名
*   类的命名采用驼峰法，并且首字母大写，例如 `User`、`UserInfo`；
*   方法的命名使用驼峰法，并且首字母小写，例如 `getUserName`；
*   属性的命名使用驼峰法，并且首字母小写，例如 `userInfo`、`instance`；

### 常量和配置
*   常量以大写字母和下划线命名，例如 `HOST_PATH`和 `VERSION`；
